package com.example.taulia.data.generator;

import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;

public class DataGeneratorUtils {
    static final String[] HEADERS = new String[]{"buyer",
            "image_name",
            "invoice_image",
            "invoice_due_date",
            "invoice_number",
            "invoice_amount",
            "invoice_currency",
            "invoice_status",
            "supplier"};

    static final String[] NAME_MAPPINGS = new String[]{"buyer",
            "imageName",
            "invoiceImage",
            "invoiceDueDate",
            "invoiceNumber",
            "invoiceAmount",
            "invoiceCurrency",
            "invoiceStatus",
            "supplier"};

    static final CellProcessor[] PROCESSORS = new CellProcessor[]{new NotNull(),
            new Optional(),
            new Optional(),
            new NotNull(),
            new NotNull(),
            new NotNull(),
            new NotNull(),
            new Optional(),
            new NotNull(),};
}
