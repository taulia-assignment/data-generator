package com.example.taulia.data.generator.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Record {
    private String buyer;
    private String imageName;
    private String invoiceImage;
    private LocalDate invoiceDueDate;
    private String invoiceNumber;
    private int invoiceAmount;
    private String invoiceCurrency;
    private String invoiceStatus;
    private String supplier;

    public Record(String buyer,
                  Image image,
                  Invoice invoice,
                  String supplier) {
        this.buyer = buyer;
        this.imageName = image.getName();
        this.invoiceImage = image.getBase64encoded();
        this.invoiceDueDate = invoice.getDueDate();
        this.invoiceNumber = invoice.getNumber();
        this.invoiceAmount = invoice.getAmount();
        this.invoiceCurrency = invoice.getCurrency();
        this.invoiceStatus = invoice.getStatus();
        this.supplier = supplier;
    }
}
