package com.example.taulia.data.generator.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Invoice {
    private LocalDate dueDate;
    private String number;
    private int amount;
    private String currency;
    private String status;
}
