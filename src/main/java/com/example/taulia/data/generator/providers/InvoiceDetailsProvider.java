package com.example.taulia.data.generator.providers;

import com.example.taulia.data.generator.model.Currency;
import com.example.taulia.data.generator.model.Invoice;
import com.example.taulia.data.generator.model.InvoiceStatus;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Random;
import java.util.UUID;

public class InvoiceDetailsProvider {

    private final Random random;

    public InvoiceDetailsProvider(Random random) {
        this.random = random;
    }

    public Invoice getInvoice() {
        return new Invoice(randomDate(),
                UUID.randomUUID().toString(),
                random.nextInt(100000),
                Currency.values()[random.nextInt(1)].toString(),
                random.nextInt(2) == 0 ?
                        "" :
                        InvoiceStatus.values()[random.nextInt(3)].toString()
        );
    }

    private LocalDate randomDate() {
        int day = random.nextInt(366);
        int year = random.nextInt(11);

        return Instant.now()
                .minus(Duration.ofDays(day * year))
                .atZone(ZoneId.of("UTC"))
                .toLocalDate();
    }
}
