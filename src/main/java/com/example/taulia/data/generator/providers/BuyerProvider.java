package com.example.taulia.data.generator.providers;

import java.util.Random;

public class BuyerProvider {
    private static final String[] BUYERS = new String[]{"South African Gold Mines Corp", "Traksas", "Axtronics"};

    private final Random random;

    public BuyerProvider(Random random) {
        this.random = random;
    }

    public String getBuyer() {
        return BUYERS[random.nextInt(3)];
    }
}
