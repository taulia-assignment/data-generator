package com.example.taulia.data.generator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@AllArgsConstructor
public class DataGeneratorProperties {
    private int numberOfRecords;
    private String fileLocation;
}
