package com.example.taulia.data.generator.model;

public enum Currency {
    USD,
    CAD
}
