package com.example.taulia.data.generator.providers;

import com.example.taulia.data.generator.model.Image;

import javax.imageio.ImageIO;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;


public class ImageProvider {
    private static final int[] IMAGE_DISTRIBUTION = new int[]{0, 0, 0, 0, 1, 1, 1, 2, 2, 3, 4, 4};

    private static final String[] IMAGE_NAMES = new String[]{"a-lobby-50-percent-resized.png",
            "a-winery-25-percent-resized.png",
            "b-site.png",
            "market.png"};
    private static final String IMAGES_FOLDER = "images/";

    private static final ClassLoader CLASS_LOADER = Thread.currentThread().getContextClassLoader();

    private final List<Image> images;
    private final Random random;

    public ImageProvider(Random random) {
        images = new ArrayList<>();
        this.random = random;
        generateImages();
    }

    public Image getImage() {
        return images.get(IMAGE_DISTRIBUTION[random.nextInt(12)]);
    }

    private byte[] getImageBytes(InputStream inputStream) {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            ImageIO.write(ImageIO.read(inputStream), "png", out);

            return out.toByteArray();
        } catch (IOException e) {
            return new byte[0];
        }
    }

    private void generateImages() {
        images.addAll(Arrays.stream(IMAGE_NAMES)
                .map(name -> {
                    byte[] imageBytes = getImageBytes(CLASS_LOADER.getResourceAsStream(IMAGES_FOLDER + name));
                    byte[] base64encoded = Base64.getEncoder().encode(imageBytes);

                    return new Image(name, new String(base64encoded));
                })
                .collect(Collectors.toList()));

        images.add(new Image("", "")); // representing the missing invoice image information in the data file
    }
}
