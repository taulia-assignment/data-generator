package com.example.taulia.data.generator.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Image {
    private String name;
    private String base64encoded;
}
