package com.example.taulia.data.generator.providers;

import java.util.Random;

public class SupplierProvider {
    private static final String[] SUPPLIERS = new String[]{"Goldie & Sons",
            "Brown Retailers",
            "United Manufacturers",
            "Silver Logistics"};

    private Random random;

    public SupplierProvider(Random random) {
        this.random = random;
    }

    public String getSupplier() {
        return SUPPLIERS[random.nextInt(4)];
    }
}
