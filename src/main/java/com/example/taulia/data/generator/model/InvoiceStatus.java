package com.example.taulia.data.generator.model;

public enum InvoiceStatus {
    NEW,
    PAID,
    VOID
}
