package com.example.taulia.data.generator;

import com.example.taulia.data.generator.model.Record;
import com.example.taulia.data.generator.providers.BuyerProvider;
import com.example.taulia.data.generator.providers.ImageProvider;
import com.example.taulia.data.generator.providers.InvoiceDetailsProvider;
import com.example.taulia.data.generator.providers.SupplierProvider;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class DataGenerator {
    public void generate(DataGeneratorProperties properties) {
        System.out.println("Start generating sample data...");

        Random random = new Random();

        BuyerProvider buyerProvider = new BuyerProvider(random);
        ImageProvider imageProvider = new ImageProvider(random);
        InvoiceDetailsProvider invoiceDetailsProvider = new InvoiceDetailsProvider(random);
        SupplierProvider supplierProvider = new SupplierProvider(random);

        File file = new File(properties.getFileLocation());
        createFileIfNotExists(file);

        try (ICsvBeanWriter beanWriter =
                     new CsvBeanWriter(new FileWriter(properties.getFileLocation()),
                             CsvPreference.STANDARD_PREFERENCE)) {
            beanWriter.writeHeader(DataGeneratorUtils.HEADERS);

            for (int i = 0; i < properties.getNumberOfRecords(); i++)
                beanWriter.write(new Record(buyerProvider.getBuyer(),
                                imageProvider.getImage(),
                                invoiceDetailsProvider.getInvoice(),
                                supplierProvider.getSupplier()),
                        DataGeneratorUtils.NAME_MAPPINGS,
                        DataGeneratorUtils.PROCESSORS);
        } catch (IOException e) {
            System.out.println("Problem occurred while creating sample data: " + e.getMessage());
        }

        System.out.println("Done. Generated sample data is now populated into the specified file.");
    }

    private void createFileIfNotExists(File file) {
        try {
            if (file.createNewFile())
                System.out.println("Successfully created sample data file at " + file.getAbsolutePath());
        } catch (IOException e) {
            System.out.println("Problem occurred while creating sample file: " + e.getMessage());
        }
    }
}
